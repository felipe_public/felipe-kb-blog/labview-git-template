# LabVIEW Project Template for Git

This is the source code for the project template dialog for creating git based projects.

## Installation

Use the VI Package Manager [VIPM](https://vipm.io) to install the package.

## Usage

```mermaid
graph LR;
id1[Open LabVIEW]-->id2[Create Project];
id2-->id3[Custom];
id3-->id4[Git Project Template];
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[BSD3](https://choosealicense.com/licenses/bsd-3-clause/)

# Author

Felipe Pinheiro Silva